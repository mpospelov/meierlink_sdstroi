#coding: UTF-8
Sdstroi::Application.routes.draw do


  #ComfortableMexicanSofa::Routing.admin(:path => '/admin')

  # .ru to .рф redirect
  constraints(:host => /6400906.ru/) do
    match "/(*path)" => redirect {|params, req|"http://6400906.xn--p1ai/" }
  end

  get '/admin', to: redirect("/cms-admin")
  namespace :comfy do
    put "set_configuration" => "configurations#set"
    resources :advertisement_blocks
    resources :partners do
      put :sort, on: :collection
    end
    resources :main_page_slides do
      collection do
        put :set_slider_time
        put :sort
        put :projects_sort
      end
    end
    resources :news
    resources :projects do
      resources :photos
      resources :slides
      resources :floors
      resources :flats
      resources :flat_images
      resources :houses
      resources :areas do
        resources :layouts do
          member{ post :add_queue }
        end
      end
    end
  end

  resource :root_page
  resources :news

  root to: 'root_page#index'

  match '/news'                     => 'news#index'
  match '/news/filter/:year/'       => 'news#index'
  match '/news/filter/:year/:month' => 'news#index'
  match '/news/:id'                 => 'news#show'


  #TODO fix project routes
  get 'projects'                => 'projects#index'
  get 'projects/all'            => 'projects#index',    scope: :all
  get 'projects/completed'      => 'projects#index',    scope: :completed
  get 'projects/change_buy'     => "projects#change_buy"
  get 'projects/:slug'          => 'projects#show',     as: :project
  get 'projects/:slug/layout'   => 'projects#layout',   as: :project_layout
  get 'projects/:slug/progress' => 'projects#progress', as: :project_progress
  get 'projects/:slug/live'     => 'projects#live',     as: :project_live
  get 'projects/:slug/buy'      => 'projects#buy',      as: :project_buy
  get 'projects/:slug/plan'     => 'projects#plan',     as: :project_plan
  get 'projects/:slug/buy/:area_id/:layout_id'    => 'projects#buy',    as: :project_buy_area_layout
  post 'projects/:slug/send_request' => 'projects#send_request', as: :send_request
  get 'projects/:slug/layout/:area_id/'           => 'projects#layout', as: :project_layout_area
  get 'projects/:slug/layout/:area_id/:layout_id' => 'projects#layout', as: :project_layout_area_layout
  get 'projects/:slug/flats/:flat_id' => 'projects#flats', as: :project_flat

  #ComfortableMexicanSofa::Routing.content(path: '/', sitemap: false)
end
