class AdvertisementBlock < ActiveRecord::Base
  attr_accessible :color, :content, :permalink, :published
end
