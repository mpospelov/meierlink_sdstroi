class ProjectArea < ActiveRecord::Base
  attr_protected :id

  mount_uploader :photo, ProjectAreaUploader

  belongs_to :project

  has_many :layouts, class_name: ProjectLayout, foreign_key: 'area_id'

  validates :name, :photo, presence: true

  def amount
    self.layouts.sum(:amount)
  end
end
