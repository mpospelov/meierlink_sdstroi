class LayoutQueue < ActiveRecord::Base

  attr_accessible :price, :layout_id

  belongs_to :layout, class_name: ProjectLayout

  def position
    RomanNumerals.to_roman(layout.queues.pluck(:id).index(self.id).to_i + 1) unless new_record?
  end

end
