class ProjectPhoto < ActiveRecord::Base
  attr_protected :id

  mount_uploader :file, ProjectPhotoUploader

  belongs_to :project

  validates :file, presence: true
end
