class MainPageSlide < ActiveRecord::Base

  attr_accessible :position, :project_id, :slide_image

  belongs_to :project

  mount_uploader :slide_image, SlideImageUploader

  validates_presence_of :project_id, :slide_image

  before_create :set_max_position

  default_scope order(:position)

  private

  def set_max_position
    position = MainPageSlide.maximum(:position)
  end

end
