class House < ActiveRecord::Base
  attr_protected :id
  belongs_to :project
  has_many :floors
  validates_presence_of :project_id, :name
end