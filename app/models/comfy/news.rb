class Comfy::News < ActiveRecord::Base
  attr_accessible :date, :header, :short_description, :description, :is_published

  validates :date, presence: true
  validates :header, presence: true
  validates :short_description, length: {maximum: 256}


  attr_accessible :logo
  has_attached_file :logo,
                    :styles => { :thumb => "234x155>" }

  scope :published, where(:is_published => true)


  def self.published_between *attrs
    date = Date.new *attrs.compact.map(&:to_i)
    query_attributes = case attrs.compact.size
                       when 1
                         [date, date.end_of_year]
                       when 2
                         [date, date.end_of_month]
                       end
    if query_attributes
      where("date >= ? AND date <= ?", *query_attributes)
    else
      self
    end.published.order("date DESC")
  end

  def self.take_year_month_hash
    ym_hash = {}
    news = Comfy::News.published
    news.each do |n|
      key = n.date.year
      if ym_hash.has_key?(key)
        ym_hash[key] << n.date.month
        ym_hash[key].sort!
        ym_hash[key].uniq!
      else
        ym_hash[key] = [n.date.month]
      end
    end
    ym_hash
  end


end
