class ProjectLayoutPhoto < ActiveRecord::Base
  attr_accessible :file

  mount_uploader :file, ProjectLayoutUploader

  belongs_to :layout, class_name: ProjectLayout

  validates :file, presence: true
end
