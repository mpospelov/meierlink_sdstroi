class Configuration < ActiveRecord::Base
  attr_accessible :name, :value

  class << self
    def [](name)
      model = where(name: name).first
      model.try :value
    end

    def []=(name, value)
      if value.nil?
        model = where(name: name).first
        model.destroy
      else
        model = where(name: name).first_or_create
        model.update_attributes(name: name, value: value)
      end
    end
  end
end
