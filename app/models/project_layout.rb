class ProjectLayout < ActiveRecord::Base
  attr_protected :id
  attr_accessible :name, :title, :amount, :content, :photos_attributes,
                  :photo_files, :queues_attributes
  validates :price, numericality: true

  has_many :photos, class_name: ProjectLayoutPhoto, foreign_key: :layout_id
  has_many :queues, class_name: LayoutQueue, foreign_key: :layout_id

  accepts_nested_attributes_for :photos, allow_destroy: true, reject_if: lambda { |a| a[:file].blank? }
  accepts_nested_attributes_for :queues, allow_destroy: true

  belongs_to :area, class_name: ProjectArea

  validates :name, presence: true

  def full_type
    "[#{area.name} м2] #{self.name}"
  end

  def name_no_br
    name.gsub("\\n","")
  end

  def price
    queues.first.try(:price) || 0
  end

  def photo_files=(photos)
    photos.each do |photo|
      self.photos.new file: photo
    end
  end
end
