class FlatImage < ActiveRecord::Base
  #accepts_nested_attributes_for :image

  attr_accessible :image, :date, :flat_id

  mount_uploader :image, FlatUploader

  belongs_to :flat
  validates :image, :date, presence: true

  def self.batch_create(flat_images, flat_id)
    images = flat_images[:image]
    images.each do |image|
      FlatImage.create(:image => image, :date => flat_images[:date], :flat_id => flat_id)
    end
  end

  def data_day
    beggining_of_slide= flat.flat_images.first.date.beginning_of_month.strftime('%j').to_i
    date.strftime('%j').to_i - beggining_of_slide
  end

end
