class Flat < ActiveRecord::Base
  States = ["buyed", "booked", "on_sell"].freeze
  StateColors = {
    "buyed" => "#d7240a", 
    "booked" => "gray", 
    "on_sell" => "green"
  }.freeze

  default_scope ->{order("created_at DESC")}
  attr_protected :id
  belongs_to :floor
  belongs_to :layout, class_name: "ProjectLayout"
  has_many :flat_images
  accepts_nested_attributes_for :flat_images
  delegate :house, :project, to: :floor
  validates_presence_of :floor_id, :number, :polygon_points
  validates :layout_id, presence: true, if: :is_on_sell?
  validates_inclusion_of :buyed, in: States

  delegate :area, to: :layout

  def is_on_sell?
    self.buyed == States[2]
  end

  def self.translated_state(state)
    I18n.t(state, scope: "flat.states")
  end

  def current_state
    Flat.translated_state(buyed)
  end

  def get_color
    StateColors[buyed]
  end

  def get_text
    "квартира №#{number}\n" <<
    case buyed
    when 'buyed'
      "Продано!\n" <<
      "Нажмите для получения\n" <<
      "информации о ходе строительства"
    when 'booked'
      "Забронировано!"
    when 'on_sell'
      "Можно купить!\n" <<
      "Нажмите для получения\n" <<
      "более подробной информации"
    end
  end

  def get_poligon_points
    self.polygon_points.split(',').map(&:to_i)
  end

end
