class Project < ActiveRecord::Base
  attr_protected :id
  before_create :set_default_main_page_slides_position

  mount_uploader :photo, ProjectUploader

  has_many :photos, class_name: ProjectPhoto
  has_many :slides, class_name: ProjectSlide
  has_many :areas,  class_name: ProjectArea
  has_many :houses
  has_many :main_page_slides

  scope :published, where(published: true)
  scope :completed, where(published: true, completed: true)
  scope :underway, where(published: true, completed: false)

  bitmask :objects, as: [:object1, :object2, :object3, :object4, :object5, :object6, :object7]

  validates :slug, :title, :category, :address, :photo, presence: true

  def buyable?
    areas.joins(:layouts).present?
  end

  def layouts
    ProjectLayout.joins(area: :project).where("projects.id = ?", self.id)
  end

  def with_plan?
    houses.present?
  end

  private

  def set_default_main_page_slides_position
    self.main_page_slides_position = Project.maximum(:main_page_slides_position) + 1
  end
end
