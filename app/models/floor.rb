class Floor < ActiveRecord::Base
  attr_protected :id
  belongs_to :house
  has_many :flats
  validates_presence_of :house_id, :image, :name
  mount_uploader :image, FloorUploader
  delegate :project, to: :house
  has_one :additional_floor, class_name: "Floor"
  belongs_to :floor
  scope :entire, ->{where(floor_id: nil)}

  def is_additional_floor?
    self.floor.present?
  end

  def first_level
    self.is_additional_floor? ? self.floor : self
  end

  def second_level
    self.is_additional_floor? ? self : self.additional_floor 
  end

  def additional_floor_id
    self.additional_floor.id.to_s if self.additional_floor.present?
  end

  def additional_floor_id=(id)
    self.additional_floor = Floor.find(id)
  end

  def floor_image
    @floor_image ||= MiniMagick::Image.open(image.path)
  end

  def get_width
    floor_image['width']
  end

  def get_height
    floor_image['height']
  end

end
