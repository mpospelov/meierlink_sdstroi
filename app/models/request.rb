class Request
  include ActiveModel::Validations
  include ActiveModel::Conversion

  extend ActiveModel::Naming

  attr_accessor :name, :email, :payment, :area_id, :layout_id, :project_id, :queue_id

  validates :name, :email, :payment, presence: true

  def initialize(attributes = {})
    if attributes
      attributes.each do |name, value|
        send("#{name}=", value)
      end
    end
  end

  def persisted?
    false
  end

  def area
    ProjectArea.find(area_id)
  end

  def layout
    ProjectLayout.find(layout_id)
  end

  def project
    Project.find(project_id)
  end

  def queue
    LayoutQueue.find(queue_id)
  end

end
