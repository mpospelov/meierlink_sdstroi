class Partner < ActiveRecord::Base

  attr_accessible :name, :slug, :image, :position

  mount_uploader :image, PartnerUploader

  validates_presence_of :name, :slug, :image

  before_create :set_position

  default_scope order(:position)

  private

  def set_position
    self.position = self.class.maximum(:position)
  end

end
