class ProjectSlide < ActiveRecord::Base
  attr_protected :id

  mount_uploader :file, ProjectSlideUploader

  belongs_to :project

  default_scope order(:date)

  validates :file, :date, presence: true

  def data_day
    beggining_of_slide = project.slides.first.date.beginning_of_month.strftime('%j').to_i
    date.strftime('%j').to_i - beggining_of_slide
  end
end
