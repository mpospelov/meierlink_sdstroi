# encoding: utf-8
class Mailer < ActionMailer::Base
  default from: "noreply@6400906.xn--p1ai"
  #default to: "mixan946@ya.ru"
  default to: "sdscompany@ya.ru"

  def request_email buy_request
    @buy_request = buy_request

    mail(subject: 'Заявка на покупку квартиры')
  end
end
