#coding: UTF-8
class Comfy::NewsController < CmsAdmin::BaseController
  # GET /comfy/news
  # GET /comfy/news.json

  def index
    @comfy_news = Comfy::News.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @comfy_news }
    end
  end

  # GET /comfy/news/1
  # GET /comfy/news/1.json
  def show
    @comfy_news = Comfy::News.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comfy_news }
    end
  end

  # GET /comfy/news/new
  # GET /comfy/news/new.json
  def new
    @comfy_news = Comfy::News.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comfy_news }
    end
  end

  # GET /comfy/news/1/edit
  def edit
    @comfy_news = Comfy::News.find(params[:id])
  end

  # POST /comfy/news
  # POST /comfy/news.json
  def create
    @comfy_news = Comfy::News.new(params[:comfy_news])

    respond_to do |format|
      if @comfy_news.save
        format.html { redirect_to @comfy_news, notice: 'Новость была успешно создана!' }
        format.json { render json: @comfy_news, status: :created, location: @comfy_news }
      else
        format.html { render action: "new" }
        format.json { render json: @comfy_news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /comfy/news/1
  # PUT /comfy/news/1.json
  def update
    @comfy_news = Comfy::News.find(params[:id])

    respond_to do |format|
      if @comfy_news.update_attributes(params[:comfy_news])
        format.html { redirect_to @comfy_news, notice: 'Новость была успешно обновлена!' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comfy_news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comfy/news/1
  # DELETE /comfy/news/1.json
  def destroy
    @comfy_news = Comfy::News.find(params[:id])
    @comfy_news.destroy

    respond_to do |format|
      format.html { redirect_to comfy_news_index_url }
      format.json { head :no_content }
    end
  end
end
