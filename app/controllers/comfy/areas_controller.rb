class Comfy::AreasController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  defaults class_name: 'ProjectArea'

  belongs_to :project
end
