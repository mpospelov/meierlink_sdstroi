#coding: utf-8
class Comfy::MainPageSlidesController < CmsAdmin::BaseController

  inherit_resources

  actions :all, except: [:show]

  def edit
    @main_page_slide = MainPageSlide.find(params[:id])
    @project = @main_page_slide.project
  end

  def new
    @project = Project.find(params[:project_id])
    @main_page_slide = @project.main_page_slides.new
  end

  def index
    @projects = Project.order(:main_page_slides_position)
  end

  def destroy
    @main_page_slide = MainPageSlide.find(params[:id])
    @main_page_slide.destroy
  end

  def sort
    params[:main_page_slide].each_with_index do |id, position|
      MainPageSlide.find(id).update_attributes!(position: position, project_id: params[:project_id])
    end
    render nothing: true
  end

  def projects_sort
    params[:project].each_with_index do |id, position|
      Project.find(id).update_attributes!(main_page_slides_position: position)
    end
    render nothing: true
  end

end
