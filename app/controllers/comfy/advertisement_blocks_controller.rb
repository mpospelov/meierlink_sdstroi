class Comfy::AdvertisementBlocksController < CmsAdmin::BaseController
  def index
    @advertisement = AdvertisementBlock.first_or_initialize
  end

  def create
    @advertisement = AdvertisementBlock.first_or_initialize
    @advertisement.update_attributes(params[:advertisement_block])
    @advertisement.save
    redirect_to comfy_advertisement_blocks_path
  end
end
