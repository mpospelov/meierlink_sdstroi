class Comfy::SlidesController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  defaults class_name: 'ProjectSlide'

  belongs_to :project

  def create
    @project = Project.find(params[:project_id])
    params[:project_slide][:file].each do |file|
      @project.slides.create!(file: file, date: params[:project_slide][:date])
    end
    redirect_to comfy_project_slides_path
  end

end
