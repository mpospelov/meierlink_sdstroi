class Comfy::LayoutsController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  defaults class_name: 'ProjectLayout'

  belongs_to :project
  belongs_to :area, class_name: 'ProjectArea'

  def edit
    resource.photos.build
    edit!
  end

  def create
    create! do |success, failure|
      success.html { redirect_to  edit_comfy_project_area_layout_path(@project, @area, @layout) }
    end
  end

  def add_queue
  end
end
