class Comfy::PhotosController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  defaults class_name: 'ProjectPhoto'

  belongs_to :project
end
