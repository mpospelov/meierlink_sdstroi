# coding: UTF-8
class Comfy::PartnersController < CmsAdmin::BaseController
  def index
    @partners = Partner.all
  end

  def new
    @partner = Partner.new
  end

  def edit
    @partner = Partner.find(params[:id])
  end

  def sort
    ids = params[:partner].map(&:to_i)
    Partner.where(id: ids).each do |partner|
      partner.update_attribute(:position, ids.index(partner.id))
    end
    render nothing: true
  end

  def update
    @partner = Partner.find(params[:id])
    if @partner.update_attributes(params[:partner])
      redirect_to comfy_partners_path, notice: "Партнер #{@partner.name} успешно изменен!"
    else
      render :edit
    end
  end

  def create
    @partner = Partner.new(params[:partner])
    if @partner.save
      redirect_to comfy_partners_path, notice: "Партнер успешно добавлен!"
    else
      render :new
    end
  end

  def destroy
    Partner.destroy(params[:id])
    redirect_to comfy_partners_path, notice: "Партнер успешно удален!"
  end
end
