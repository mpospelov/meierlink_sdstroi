class Comfy::HousesController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  belongs_to :project
end