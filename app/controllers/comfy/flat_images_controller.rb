class Comfy::FlatImagesController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  belongs_to :flat

  def destroy
    super do |format|
      format.html { redirect_to comfy_project_flat_images_url(flat_id: params[:flat_id]) }
    end
  end
  def create
    FlatImage.batch_create(params[:flat_image], params[:flat_id])
    redirect_to comfy_project_flat_images_url(flat_id: params[:flat_id])
  end
  def update
    super do |format|
      format.html { redirect_to comfy_project_flat_images_url(flat_id: params[:flat_id]) }
    end
  end
end