class Comfy::ConfigurationsController <  CmsAdmin::BaseController

  def set
    params[:configuration].each do |k,v|
      ::Configuration[k] = v
    end
    redirect_to request.referer
  end

end
