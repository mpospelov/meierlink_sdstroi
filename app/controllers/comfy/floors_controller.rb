class Comfy::FloorsController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  belongs_to :house

  def new
    super do |format|
      format.html
      format.js
    end
  end

  def destroy
    super do |format|
      format.html { redirect_to comfy_project_floors_url(house_id: params[:house_id]) }
    end
  end
  def create
    super do |format|
      format.html { redirect_to comfy_project_floors_url(house_id: params[:house_id])}
    end
  end
  def update
    super do |format|
      format.html { redirect_to comfy_project_floors_url(house_id: params[:house_id]) }
    end
  end

end
