class Comfy::FlatsController < CmsAdmin::BaseController
  inherit_resources

  actions :all, except: [:show]

  belongs_to :floor

  def new
    super do |format|
      format.html
      format.js
    end
  end

  def destroy
    super do |format|
      format.html { redirect_to comfy_project_flats_url(floor_id: params[:floor_id]) }
    end
  end

  def create
    super do |format|
      format.html { redirect_to comfy_project_flats_url(floor_id: params[:floor_id]) }
    end
  end

  def update
    @flat = Flat.find(params[:id])
    if params[:polygon_points]
      @flat.update_attributes(polygon_points: params[:polygon_points].join(","))
    end
    @flat.update_attributes(params[:flat])
    respond_to do |format|
      format.html{redirect_to comfy_project_flats_url(floor_id: @flat.floor_id)}
      format.js{render nothing: true}
    end
  end

end
