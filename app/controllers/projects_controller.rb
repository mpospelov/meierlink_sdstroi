class ProjectsController < ApplicationController
  before_filter :find_project, except: [:index, :change_buy]

  def index
    @collection = case params[:scope]
                    when :all
                      Project.published
                    when :completed
                      Project.completed
                    else
                      Project.underway
                  end
  end

  def show
  end

  def live
  end

  def change_buy
    @project = Project.find(params[:id])
    @request = Request.new
    @area   = (@project.areas.find(params[:area_id]) rescue @project.areas.first)
    @layout = (@area.layouts.find(params[:layout_id]) rescue @area.layouts.first)
  end

  def send_request
    @request = Request.new(params[:request])
    if @request.valid?
      Mailer.request_email(@request).deliver
    end
  end

  def buy
    @request = Request.new
    if @project.buyable?
      @area   = params[:area_id] ? @project.areas.find(params[:area_id]) : @project.areas.first
      @layout = params[:layout_id] ? @area.layouts.find(params[:layout_id]) : @area.layouts.first
    end
  end

  def layout
    @area = params[:area_id].present? ? @project.areas.find(params[:area_id]) : @project.areas.first
    if @area
      @layout = params[:layout_id].present? ? @area.layouts.find(params[:layout_id]) : @area.layouts.first
    end
    @areas  = @project.areas.select { |r| r.layouts.count > 0 }
  end

  def progress
  end

  def plan
    if params[:house_id] || !params[:floor_id]
      @houses = @project.houses
      @current_house = @project.houses.find_by_id(params[:house_id]) || @houses.first
      
      @floors = @current_house.floors.entire
      @current_floor = @floors.find_by_id(params[:floor_id]) || @floors.first
    else
      @current_floor = Floor.find(params[:floor_id])
      @current_house = @current_floor.house
      @floors = @current_house.floors.entire
    end
  end

  def flats
    @flat = Flat.find(params[:flat_id])
  end

  private
  def find_project
    @project = Project.find_by_slug! params[:slug]
  end
end
