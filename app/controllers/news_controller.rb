class NewsController < ApplicationController

  def index
    @news = Comfy::News.published_between(params[:year], params[:month])
                       .paginate(page: params[:page], per_page: ::Configuration[:news_per_page])
    render cms_page: '/news'
  end

  def show
    @single_news = Comfy::News.find(params[:id])
    render :cms_page => '/news'
  end

end
