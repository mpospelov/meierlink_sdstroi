class RootPageController < ApplicationController
  def index
    @projects = Project.published.order(:main_page_slides_position)

    @main_page_slides = @projects.map(&:main_page_slides).flatten

    @news = Comfy::News.order(:date).last(3)

    render :cms_page => '/'
  end
end
