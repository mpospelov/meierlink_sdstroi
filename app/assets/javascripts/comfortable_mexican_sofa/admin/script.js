$(function(){
  $.validate();
  $('.colorpicker').colorpicker();
  $('.wysihtml5').wysihtml5({locale: "ru-RU", image: false, lists: false})
});

$.toogleSlideForms = function(){
    $("#new-slide").remove();
    $('#edit-slide').remove();
    $('.hidden-slide').removeClass('hidden-slide');
}
$.formValid = function(){
    if($("#main_page_slide_slide_image").val() == ""){
        alert("Пустое поле 'Изображение'!");
        return false;
    }
    return true;
}

window.CMS.wysiwyg = function() {
  var csrf_param, csrf_token, params;
  csrf_token = $('meta[name=csrf-token]').attr('content');
  csrf_param = $('meta[name=csrf-param]').attr('content');
  if (csrf_param !== void 0 && csrf_token !== void 0) {
    params = csrf_param + "=" + encodeURIComponent(csrf_token);
  }

  $('textarea[data-rich-text]').redactor({
    lang: 'ru',
    minHeight: 400,
    tidyHtml: false,
    buttons: ['html','formatting', 'q', '|', 'bold', 'italic', 'deleted', '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', '|', 'image', 'video', 'file', 'table', 'link', '|', '|', 'alignment', '|', 'horizontalrule'],
    imageUpload: "" + CMS.file_upload_path + "?ajax=1&" + params,
    imageGetJson: "" + CMS.file_upload_path + "?ajax=1",
    formattingTags: ['p', 'h1', 'h2', 'h3', 'h4', "blockquote"],
    plugins: ["clips", "fullscreen"],
    buttonsCustom: {
        q: {
            title: 'q',
            callback: function(buttonName, buttonDOM, buttonObject) {
                this.formatBlocks('q');
            }
        },
    },
    convertDivs: false,
  });
};
