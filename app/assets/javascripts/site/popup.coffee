$.fn.popup = ->
  popup = $(this)
  popup.html "<span>?</span><div class='popup-content'>" +
    popup.html() +
    "</div>"
  content = popup.find(".popup-content")

  $("body").on 'mouseover', popup.selector, ->
    showContent()
  $("body").on 'mouseleave', popup.selector, ->
    hideContent()

  showContent = ->
    content.stop()
    popup.stop()
    popup.animate {width: 280}, complete: ->
      $("#target").append("<p>All done.</p>");
      content.slideDown()

  hideContent = ->
    content.stop()
    popup.stop()
    content.slideUp {complete: ->
      popup.animate width: 20
    }
