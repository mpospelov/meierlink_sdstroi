class @Slider
  constructor: ->
    @delay_time = 5000
    @current_slide = 0
    @stop_flag = true
    @slides = $('.slider-thumbs > a').map ->
      new Slide $(this)
    @slides_length = @slides.length
    @go()

  start: ->
    @stop_flag = false
    true

  stop: ->
    @stop_flag = true

  go: ->
    window.setInterval (=>
      @next()
    ), @delay_time


  next: ->
    unless @stop_flag
      @current_slide = (@current_slide + 1) % @slides_length
      @slides[@current_slide].activate()

class @Slide

  constructor:(@thumb) ->
    @base_image_container = $('.project-slide img')

  activate: ->
    $('.active-thumb').removeClass('active-thumb')
    @thumb.find('img').addClass('active-thumb')
    $('.project-slide').backstretch(@thumb.attr('href'), {fade: 0})

  deactivate: ->
    @thumb.find('img').removeClass('active-thumb')

