setDisabledArrows = ()->
  setTimeout ->
    if $(".pagination a:first").hasClass("selected")
      $(".project-areas .arrows .left-arrow").addClass("disabled")
      $(".project-areas .arrows .right-arrow").removeClass("disabled")

    if $(".pagination a:last").hasClass("selected")
      $(".project-areas .arrows .left-arrow").removeClass("disabled")
      $(".project-areas .arrows .right-arrow").addClass("disabled")
  ,10

change_buy_ajax = ->
  id = $("#request_project_id").val()
  area_id = $('input[name="request[area_id]"]:checked').val();
  layout_id = $("#layout_id").val()
  $.ajax
    dataType: "JSONP",
    url: "/projects/change_buy",
    data:{
      id: id,
      area_id: area_id,
      layout_id: layout_id
    }

ready = ->
  $("body").on "change", ".level input[type=radio]", ->
    $.ajax
      url: ""
      type: "GET"
      dataType: "JSONP"
      data: 
        floor_id: $(this).val()
    
  # move like krasnovo
  # $floor = $("#floor")
  # $floor.mousemove (event)-> 
  #   center = $floor.width()/2
  #   moveTo = event.pageX - center 
  #   $floor[0].scrollLeft = moveTo * $floor[0].scrollWidth/$floor[0].offsetWidth

  $("body").on "change", "#house_id", ->
    $.ajax
      url: ""
      type: "GET"
      dataType: "JSONP"
      data: 
        house_id: $(this).val()
  $("#prices .project-layout-price:first-child").show()
  $("body").on "change", ".radio.ajax input[type=radio]", ->
    change_buy_ajax()
  $("body").on "change", "#request_project_id", ->
    change_buy_ajax()
  $("body").on "change", "#request_queue_id", ->
    id = $(this).val()
    $("#prices .project-layout-price").hide()
    $("#layout_queue_" + id).show()

  $(".popup").popup()
  $(".right-arrow").mouseup ->
    setDisabledArrows()
  $(".left-arrow").mouseup ->
    setDisabledArrows()
  $(".pagination a").mouseup ->
    setDisabledArrows()

  $('.areas.desktop').carouFredSel
    circular: false,
    infinite: false,
    items: 3,
    auto: false,
    prev: '.arrows .left-arrow',
    next: '.arrows .right-arrow',
    pagination:
      container: ".pagination",
      items: 3
  if $(".areas.mobile").is(":visible")
    $('.areas.mobile').carouFredSel
      circular: false,
      infinite: false,
      items: 2,
      auto: false,
      prev: '.arrows .left-arrow',
      next: '.arrows .right-arrow',
      pagination:
        container: ".pagination",
        items: 2
      swipe:
        onMouse: true,
        onTouch: true

  $('input#name').data('holder',$('input#name').attr('placeholder'));
  $('input#name').focusin ->
    $(this).attr('placeholder','')
  $('input#name').focusout ->
    $(this).attr('placeholder',$(this).data('holder'))

  $('input#email').data('holder',$('input#email').attr('placeholder'));
  $('input#email').focusin ->
    $(this).attr('placeholder','')
  $('input#email').focusout ->
    $(this).attr('placeholder',$(this).data('holder'))

  $("select#project_slug").on 'change', ->
    $('#result').load window.location + '.js', { project_slug: $(this).val() }

  $(document).on 'change', 'select#area_id' , ->
    $('#result').load window.location + '.js', $("#form_buy").serialize()

  $('.project-layout-photo').on 'click', ->
    $('.project-layout-photos li a').colorbox({
      photo: true,
      rel:'group1',
      maxHeight: "100%",
      maxWidth: "100%",
      scalePhotos: true,
      open: true
    })
    $('.project-layout-photos li.selected a').click()
    false

  $('.project-layout-photos li').on 'click', ->
    $('.project-layout-photos li').removeClass('selected')
    $(this).addClass('selected')
    $('.project-layout-photo').attr('src', $(this).find('img').data('photo'))

    return if ($("#colorbox").css("display")=="block")
      true
    else
      false


  $('.slider-thumbs a img').on 'click', ->
    $('.thumb').removeClass('active-thumb')
    $(this).addClass('active-thumb')
    false

  $('.objects .show .project-slide').on 'click', ->
    projectSlider.stop()
    first = true
    $('.slider-thumbs a').colorbox({
      photo: true,
      rel:'group1',
      transition: 'none',
      scalePhotos: true,
      maxWidth: '100%',
      maxHeight: "100%",
      open: true,
      onClosed: ->
        projectSlider.start()
    })

    false

$(document).ready(ready)
