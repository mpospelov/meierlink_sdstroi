function equalHeight(group) {
  tallest = 0;
  group.each(function() {
    thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
}
$(document).ready(function () {
  $('.adv-popup').on('click', '.close-btn', function(){
    $.cookie("popup_hidden", 1, {path: "/"});
    $('.adv-popup').fadeOut();
  });

  if($(".adv-popup a").attr("href") == window.location.pathname){
    $.cookie("popup_hidden", 1, {path: "/"});
  }

  if($.cookie("popup_hidden") == "1"){
    $('.adv-popup').hide();  
  }
    $(".award .img a").fancybox();
    equalHeight($(".news-text"));
    equalHeight($(".press-section h5"));
    if (
        ($('#news_menu').size() > 0)
            &&
            ($('#news_list').size() > 0)) {
        var current_link = document.URL;
        var filter_ndx = current_link.split('/').indexOf('filter');

        if (filter_ndx == -1) {
            $('.year-menu-link').last().css('color', 'grey');
            $('.year-menu-link').last().css('text-decoration', 'none');
        } else {
            var year = current_link.split('/')[filter_ndx + 1];
            var month = current_link.split('/')[filter_ndx + 2];


            $('.year-menu-link:contains("' + year + '")')
                .css('color', 'grey')
                .css('text-decoration', 'none');

            $('a[href = "' + $(location).attr('pathname') + '"]').parent().addClass("active");
        }
    }
});
$(document).ajaxStart(function(){
  $("html").attr("style","cursor: wait !important;");
})
$( document ).ajaxStart(function() {
  $("html").attr("style","");
});


