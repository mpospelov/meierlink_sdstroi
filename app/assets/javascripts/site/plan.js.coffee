class @Plan
  constructor: (attrs)->
    @_initConfig(attrs)
    @_initLayers()
    @_initImages()
    @_initTooltip()
    @_initStage()
    @_initThumbnailStage() if @mainConfig.thumbSrc?
    @_initEditableMode() if @mainConfig.editable

  drawFlat: (attrs)->
    poly = new Kinetic.Line
      points: attrs.drawPoints
      fill: attrs.color
      opacity: @mainConfig.initOpacity
      closed: true
      
    poly.borderStroke = new Kinetic.Line
      points: attrs.drawPoints
      stroke: attrs.color
      strokeWidth: 4
      closed: true
      visible: false
    poly.flatName = attrs.name
    poly.onclickPath = attrs.onclickPath
    poly.updatePath = attrs.updatePath if @mainConfig.editable

    @_addEventListenerTo(poly);

    @layer.add poly.borderStroke
    @layer.add poly
    @layer.draw() if attrs.draw

  #private
  
  _initThumbnailStage: ->
    @_drawFloorThumb()
    @thumbnailLayer = new Kinetic.Layer()
  
  _initEditableMode: ->
    @newPolygonLayer = new Kinetic.Layer()
    @newPoligonCircles = []
    @stage.add @newPolygonLayer

    @keyEnterListener = (evt) =>
      if evt.keyCode is 13
        @listenEnter = false
        document.getElementById("flat_polygon_points").value = @newPolygon.getPoints()
        window.removeEventListener 'keyup', @keyEnterListener, false

    @keyBackspaceListener = (evt) =>
      if evt.keyCode is 91 || evt.keyCode is 46
        last_circle = @newPoligonCircles[@newPoligonCircles.length - 1]
        last_circle.hide()
        @newPoligonCircles.pop()
        @_redrawNewPolygon()
        window.removeEventListener 'keyup', @keyBackspaceListener, false if @newPoligonCircles.length is 0

    @newPolygon = new Kinetic.Line
      points: []
      fill: '#00D2FF'
      stroke: 'black'
      strokeWidth: 1
      opacity: @mainConfig.hoverOpacity
      closed: true

    @stage.on 'click', (evt)=>
      if evt.shiftKey
        window.addEventListener 'keyup', @keyEnterListener, false unless @listenEnter
        window.addEventListener 'keyup', @keyBackspaceListener, false
        @listenEnter = true
        mousePos = @stage.getPointerPosition()
        @_drawEditCircle(mousePos.x, mousePos.y)
        @newPolygonLayer.draw()

    @newPolygonLayer.add @newPolygon
    @newPolygonLayer.draw()

  _drawEditCircle: (x, y)->
    circle = new Kinetic.Circle
      x: x
      y: y
      radius: 4
      fill: 'red'
      stroke: 'black'
      strokeWidth: 1
      draggable: true
    circle.on 'dragend', =>
      @_redrawNewPolygon()
    @newPoligonCircles.push(circle)
    @newPolygon.getPoints().push circle.getX(), circle.getY()
    @newPolygonLayer.add(circle)
    
  _redrawNewPolygon: ->
    @newPolygon.setPoints([])
    for circle in @newPoligonCircles
      @newPolygon.getPoints().push circle.getX(), circle.getY()
    @newPolygonLayer.batchDraw()

  _drawCirclesFromPolygon: (polygon)->
    for circle in @newPoligonCircles
      circle.hide()
    @newPoligonCircles = []
    @newPolygon.points([])
    i = 0
    points = polygon.points()
    while i < points.length
      x = points[i]
      y = points[i + 1]
      @_drawEditCircle(x, y)
      i += 2

  _initStage: ->
    self = @

    @stage = new Kinetic.Stage
      container: @mainConfig.container
      width: @mainConfig.width
      height: @mainConfig.height

    @domContainer.addEventListener 'scroll', ->
      stroke_position = self.thumbnailStroke.getPosition()
      stroke_position.x = self.thumbnailImage.attitude * self.domContainer.scrollLeft
      self.thumbnailStroke.setPosition(stroke_position)
      self.thumbnailLayer.batchDraw()

    @stage.add @imageLayer
    @stage.add @layer
    @stage.add @tooltipLayer

  _initImages: ->
    @_drawFloorPlan()

  _initLayers: ->
    @domContainer = document.getElementById(@mainConfig.container)
    @layer = new Kinetic.Layer()
    @imageLayer = new Kinetic.Layer()
    @tooltipLayer = new Kinetic.Layer()

  _initTooltip: -> 
    @mainTooltip = new Kinetic.Label
      opacity: 0.9
      visible: false
      listening: false

    @mainTooltip.add(new Kinetic.Tag 
      pointerDirection: 'down'
      pointerWidth: 10
      pointerHeight: 10
      cornerRadius: 5
      shadowColor: 'black',
      shadowBlur: 10
      shadowOffset: 
        x:10
        y:20
      shadowOpacity: 0.5
    )

    @mainTooltip.add(new Kinetic.Text
      text: ""
      fontFamily: 'Calibri'
      fontSize: 18
      padding: 5
      align: 'center'
      fill: 'white'
    )
    @tooltipLayer.add @mainTooltip
    @tooltipLayer.draw()

  _initConfig: (attrs)->
    @mainConfig = attrs
    @mainConfig.hoverOpacity ||= 1
    @mainConfig.initOpacity ||= 0

  _drawFloorThumb: ->
    imageObj = new Image()
    imageObj.src = @mainConfig.thumbSrc
    self = @
    imageObj.onload = -> 
      self.thumbnailImage = new Kinetic.Image
        x: 0
        y: 0
        image: imageObj
      self.thumbnailImage.sizes = {width: @width, height: @height}
      self.thumbnailImage.attitude = @width / self.mainConfig.width
      visible_width = document.getElementById(self.mainConfig.container).offsetWidth
      strokeWidth = self.thumbnailImage.attitude * visible_width
      self.thumbnailStroke = new Kinetic.Rect
        x: 0
        y: 0
        width: strokeWidth
        height: @height
        stroke: '#fc0101'
        strokeWidth: 4
        draggable: true
        dragBoundFunc: (pos) ->
          new_position = 
            x: Math.min self.thumbnailImage.sizes.width - strokeWidth, Math.max(0, pos.x)
            y: 0
          self.domContainer.scrollLeft = (1/self.thumbnailImage.attitude) * (new_position.x)
          new_position

      container = document.getElementById(self.mainConfig.thumbContainer)
      self.thumbnailStroke.on 'mouseover', ->
        container.style.cursor = 'ew-resize'
      self.thumbnailStroke.on 'mouseout', ->
        container.style.cursor = 'default'

      self.thumbnailStage = new Kinetic.Stage
        container: self.mainConfig.thumbContainer
        width: self.thumbnailImage.sizes.width
        height: self.thumbnailImage.sizes.height
      
      self.thumbnailStage.on 'click', (evt)->
        pointer_position = self.thumbnailStage.getPointerPosition()
        new_position = self.thumbnailStroke.getPosition()
        centerThumb = pointer_position.x - self.thumbnailStroke.getWidth()/2
        max_x = self.thumbnailImage.getWidth() - self.thumbnailStroke.getWidth()
        new_x = Math.min(max_x, Math.max(0, centerThumb))
        new_position.x = new_x
        self.thumbnailStroke.setPosition(new_position)
        self.domContainer.scrollLeft = (1/self.thumbnailImage.attitude) * (new_position.x)

        self.thumbnailLayer.draw()

      self.thumbnailStage.add self.thumbnailLayer
      self.thumbnailLayer.add(self.thumbnailImage)
      self.thumbnailLayer.add(self.thumbnailStroke)
      self.thumbnailLayer.draw()


  _drawFloorPlan: ->
    imageObj = new Image()
    imageObj.src = @mainConfig.src
    imageObj.onload = => 
      @planImage = new Kinetic.Image
        x: 0
        y: 0
        image: imageObj
        width: @mainConfig.width
        height: @mainConfig.height
      @imageLayer.add(@planImage)
      @imageLayer.draw()

  _addEventListenerTo: (polygon)->
    polygon.on 'mouseover', (evt)=>
      polygon.borderStroke.show()
      @domContainer.style.cursor = 'pointer'
      @mainTooltip.getText().setText(polygon.flatName)
      @mainTooltip.getTag().fill(polygon.fill())
      @mainTooltip.show()
      @layer.batchDraw()

    polygon.on 'mouseout', (evt)=>
      polygon.borderStroke.hide()
      @mainTooltip.hide()
      @layer.batchDraw()
      @tooltipLayer.batchDraw()
      @domContainer.style.cursor = "default" 

    polygon.on 'mousemove', (evt)=>
      mousePos = @stage.getPointerPosition()
      x = mousePos.x
      y = mousePos.y - 5
      @mainTooltip.setPosition x: x, y: y
      @tooltipLayer.batchDraw()

    polygon.longClickCallback = =>
      keyupCallback = (evt)=>
        if evt.keyCode is 13
          $.ajax
            method: "PUT"
            dataType: "JSONP"
            url: polygon.updatePath
            data:
              polygon_points: @newPolygon.points()
            complete: ->
              window.location.reload(true)
              window.removeEventListener('keyup', keyupCallback, false)
              
      polygon.longClick = true
      @_drawCirclesFromPolygon(polygon)
      @_redrawNewPolygon()
      @layer.draw()
      @newPolygonLayer.draw()
      window.addEventListener('keyup',keyupCallback, false)

    if @mainConfig.editable
      polygon.longClick = false
      polygon.on 'mousedown', (evt)=>
       polygon.longClickTimeout = setTimeout(polygon.longClickCallback, 1000)

    polygon.on 'mouseup', (evt)=>
      clearTimeout(polygon.longClickTimeout)
      unless polygon.longClick
        if evt.metaKey || evt.ctrlKey
          win = window.open(polygon.onclickPath, '_blank')
          win.focus()
        else if !evt.shiftKey
          window.location = polygon.onclickPath
