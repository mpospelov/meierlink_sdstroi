class @ProgressSlider
  constructor: ->
    @slider = $("#slider")
    @thumbs = $('.slider-thumbs image-data')

    @max_value = @slider.slider( "option", "max" )
    @current_slide = @slider.slider("value")
    @previous_slide = @current_slide

    @left_arrow = $(".slide-arrows .left-arrow")
    @right_arrow = $(".slide-arrows .right-arrow")

    @left_arrow.disable = @disable
    @right_arrow.disable = @disable

    @left_arrow.enable = =>
      data_thumb = @thumbs[@current_slide - 1]
      next_date = tooltip.find('div').text($(data_thumb).attr("alt"))
      $(@left_arrow).find(".tooltip").text(next_date.text())
      $(@left_arrow).removeClass("disabled")

    @right_arrow.enable = =>
      data_thumb = @thumbs[@current_slide + 1]
      next_date = tooltip.find('div').text($(data_thumb).attr("alt"))
      $(@right_arrow).find(".tooltip").text(next_date.text())
      $(@right_arrow).removeClass("disabled")

    @left_arrow.click =>
      current_value = @slider.slider("value")
      @slider.slider("value", current_value - 1)
      @slider.trigger("slide")

    @right_arrow.click =>
      current_value = @slider.slider("value")
      @slider.slider("value", current_value + 1)
      @slider.trigger("slide")

    @slider.on "slide", (event, ui)=>
      @next(event, ui)

    @update_backstretch()

  update_backstretch: ->

    if @current_slide == @max_value
      @right_arrow.disable()
      @left_arrow.enable()
    else if @current_slide == 0
      @left_arrow.disable()
      @right_arrow.enable()
    else
      @left_arrow.enable()
      @right_arrow.enable()

    data_thumb = @thumbs[@current_slide]
    tooltip.find('div').text($(data_thumb).attr("alt"))
    tooltip.show()
    $(".backstretch").remove()
    $('.backstretch-container').backstretch($(data_thumb).attr("src"), {fade: 0})
    tooltip.hide()


  next: (event, ui)->
    value = (if !!ui then ui.value else @slider.slider("value"))
    @previous_slide = @current_slide unless @current_slide == value
    @current_slide = value
    @update_backstretch() unless @previous_slide == @current_slide

  disable: () ->
    $(this).find(".tooltip").text("")
    $(this).addClass("disabled")

$ ->
  if $("#slider").length != 0
    new ProgressSlider
