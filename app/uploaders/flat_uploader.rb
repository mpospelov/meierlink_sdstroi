# encoding: utf-8
class FlatUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "system/projects/flats/#{model.id}"
  end

  version :admin do
    process resize_to_fit: [100, 100]
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{@name}.#{file.extension}"
    end
  end

end