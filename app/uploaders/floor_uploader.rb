# encoding: utf-8
class FloorUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "system/projects/floors/#{model.id}"
  end

  version :admin do
    process resize_to_fit: [100, 100]
  end

  version :thumb do
    process resize_to_fill: [600, 0]
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{@name}.#{file.extension}"
    end
  end

end
