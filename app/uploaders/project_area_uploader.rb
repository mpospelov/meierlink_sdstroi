# encoding: utf-8
class ProjectAreaUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "system/projects/#{model.project.id}/area"
  end

  version :thumb do
    process resize_to_limit: [270, 220]
  end

  version :admin do
    process resize_to_fit: [100, 100]
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{@name}.#{file.extension}"
    end
  end
end
