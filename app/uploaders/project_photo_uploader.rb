# encoding: utf-8
class ProjectPhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  #include CarrierWave::ImageOptimizer

  storage :file

  def store_dir
    "system/projects/#{model.project.id}/photos/#{model.id}"
  end

  version :admin do
     process resize_to_fit: [100, 100]
  end

  version :normal do
    process quality: 70
    process resize_to_fill: [1280, 640]
    #process :optimize
  end

  version :thumb do
    process quality: 40
    process resize_to_fill: [111, 71]
    #process :optimize
  end

end
