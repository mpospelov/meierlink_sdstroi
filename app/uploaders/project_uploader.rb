# encoding: utf-8
class ProjectUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  #include CarrierWave::ImageOptimizer
  include CarrierWave::MiniMagick

  process quality: 60

  storage :file

  def store_dir
    "system/projects/#{model.id}"
  end

  version :normal do
    process quality: 60
    process resize_to_fill: [640, 470]
    #process :optimize
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :scale => [50, 50]
  # end


  def filename
     "#{mounted_as}#{File.extname(@filename)}" if original_filename
  end
end
