# encoding: utf-8
class ProjectLayoutUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "system/projects/#{model.layout.area.project.id}/layout"
  end

  version :thumb do
    process resize_to_fill: [101, 68]
  end

  version :photo do
    process resize_to_fill: [560, 560]
  end

  version :admin do
    process resize_to_fit: [100, 100]
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{@name}.#{file.extension}"
    end
  end
end
