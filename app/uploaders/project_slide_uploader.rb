# encoding: utf-8
class ProjectSlideUploader < CarrierWave::Uploader::Base
  #include CarrierWave::ImageOptimizer
  include CarrierWave::MiniMagick

  storage :file

  def store_dir
    "system/projects/#{model.project.id}/slides/#{model.id}"
  end

  version :admin do
    process resize_to_fit: [100, 100]
  end

  version :normal do
    process quality: 60
    process resize_to_fill: [960, 720]
    #process :optimize
  end

end
