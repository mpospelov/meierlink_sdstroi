#encoding: utf-8
module ApplicationHelper
  def page_title title=nil
    @_page_title = title unless nil
    @_page_title ||= @cms_page.label if @cms_page
    [@_page_title, 'Строительная компания «Союз Долевого Строительства»'].compact.join '—'
  end

  def add_a_tag(page)
    '<li><a href="' + page.full_path + '">' + page.label.to_s + '</a><li>'
  end

  def get_page_name
    page = @cms_page

    label = add_a_tag(page) + '--'

    while page.parent
      page = page.parent
      label += (add_a_tag(page) + '--')
    end

    label.split('--').reverse.join('&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;')
  end

  def active_menu_class menu_class=nil
    @active_menu_class = menu_class unless menu_class.nil?
    @active_menu_class ||= request.path[/\/[^\/]*\/?/][/[^\/]+/]
    @active_menu_class
  end

  def flat_plan_path(flat)
    case flat.buyed
    when 'buyed' then project_flat_path(@project.slug, flat)
    when 'on_sell' then project_buy_area_layout_path(@project.slug, flat.area.id, flat.layout.id)
    when 'booked' then "#nil"
    end
  end

  def month_num_to_name(num)
    case num
      when 1
        'Январь'
      when 2
        'Февраль'
      when 3
        'Март'
      when 4
        'Апрель'
      when 5
        'Май'
      when 6
        'Июнь'
      when 7
        'Июль'
      when 8
        'Август'
      when 9
        'Сентябрь'
      when 10
        'Октябрь'
      when 11
        'Ноябрь'
      when 12
        'Декабрь'
    end

  end

end
