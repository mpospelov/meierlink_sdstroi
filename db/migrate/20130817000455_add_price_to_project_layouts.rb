class AddPriceToProjectLayouts < ActiveRecord::Migration
  def change
    add_column :project_layouts, :price, :decimal, :precision => 10, :scale => 2
  end
end
