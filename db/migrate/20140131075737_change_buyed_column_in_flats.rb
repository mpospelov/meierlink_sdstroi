class ChangeBuyedColumnInFlats < ActiveRecord::Migration
  def change
    change_column :flats, :buyed, :string
  end
end
