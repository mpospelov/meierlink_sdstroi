class RemoveProjectMainPhoto < ActiveRecord::Migration
  def up
    remove_column :projects, :photo_main
  end

  def down
    add_column :projects, :photo_main
  end
end
