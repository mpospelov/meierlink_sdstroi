class CreateProjectPhotos < ActiveRecord::Migration
  def change
    create_table :project_photos do |t|
      t.references :project

      t.string :file

      t.timestamps
    end
  end
end
