class AddSlidesTimeConfiguration < ActiveRecord::Migration
  def up
    Configuration[:slides_time] = 5
  end

  def down
    Configuration[:slides_time] = nil
  end
end
