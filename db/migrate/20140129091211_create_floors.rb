class CreateFloors < ActiveRecord::Migration
  def change
    create_table :floors do |t|
      t.integer :project_id
      t.string  :name
      t.string  :image
      t.timestamps
    end

    create_table :flats do |t|
      t.integer :floor_id
      t.string  :number
      t.string  :polygon_points
      t.boolean :buyed
      t.timestamps
    end

    create_table :flat_images do |t|
      t.integer :flat_id
      t.string  :image
      t.timestamps
    end

  end
end
