class CreateProjectQueues < ActiveRecord::Migration
  def change
    create_table :layout_queues do |t|
      t.string :price, default: 0
      t.integer :layout_id
      t.integer :position, default: 1

      t.timestamps
    end
  end
end
