class AddAreaIdToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :area_id, :integer
  end
end
