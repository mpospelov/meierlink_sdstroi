class CreateProjectAreas < ActiveRecord::Migration
  def change
    create_table :project_areas do |t|
      t.string :name

      t.timestamps
    end
  end
end
