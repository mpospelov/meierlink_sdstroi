class AddMainPageSlidesPositionToProjects < ActiveRecord::Migration
  def up
    add_column :projects, :main_page_slides_position, :integer,default: 0
  end

  def down
    remove_column :projects, :main_page_slides_position
  end
end
