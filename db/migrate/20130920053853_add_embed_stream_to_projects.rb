class AddEmbedStreamToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :embed_stream, :string
  end
end
