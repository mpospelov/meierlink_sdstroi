class AddLayoutIdToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :layout_id, :integer
  end
end
