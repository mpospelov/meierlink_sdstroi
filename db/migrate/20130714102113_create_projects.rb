class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string  :slug
      t.string  :title
      t.string  :category

      t.string  :photo

      t.text    :address
      t.text    :description
      t.text    :content
      t.text    :location

      t.boolean :completed, default: 0
      t.boolean :published, default: 0

      t.timestamps
    end
  end
end
