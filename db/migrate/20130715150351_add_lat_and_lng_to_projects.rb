class AddLatAndLngToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :lat, :string
    add_column :projects, :lng, :string

    add_column :projects, :location_content, :text
  end
end
