class CreateAdvertisementBlocks < ActiveRecord::Migration
  def change
    create_table :advertisement_blocks do |t|
      t.string :color
      t.text :content
      t.string :permalink

      t.timestamps
    end
  end
end
