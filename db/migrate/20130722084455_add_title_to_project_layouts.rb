class AddTitleToProjectLayouts < ActiveRecord::Migration
  def change
    add_column :project_layouts, :title, :string
    add_column :project_layouts, :content, :text

    add_column :projects, :layout, :text
  end
end
