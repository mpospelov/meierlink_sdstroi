class AddDescriptionToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :description, :text
  end
end
