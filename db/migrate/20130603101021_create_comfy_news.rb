class CreateComfyNews < ActiveRecord::Migration
  def change
    create_table :comfy_news do |t|
      t.date :date
      t.text :short_description
      t.text :description
      t.boolean :is_published

      t.timestamps
    end
  end
end
