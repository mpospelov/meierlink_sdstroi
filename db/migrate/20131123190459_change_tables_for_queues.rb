class ChangeTablesForQueues < ActiveRecord::Migration

  def change
    change_table :project_layouts do |t|
      t.remove :price
      t.remove :price_2
      t.remove :price_3
    end
    remove_column :layout_queues, :position
  end

end
