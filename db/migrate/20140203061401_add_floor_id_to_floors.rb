class AddFloorIdToFloors < ActiveRecord::Migration
  def change
    add_column :floors, :floor_id, :integer
  end
end
