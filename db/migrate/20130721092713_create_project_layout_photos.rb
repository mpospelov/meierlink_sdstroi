class CreateProjectLayoutPhotos < ActiveRecord::Migration
  def change
    create_table :project_layout_photos do |t|
      t.references :layout
      t.string :file

      t.timestamps
    end
  end
end
