class AddPhotoMainToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :photo_main, :string
  end
end
