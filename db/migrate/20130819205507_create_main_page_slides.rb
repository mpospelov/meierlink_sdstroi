class CreateMainPageSlides < ActiveRecord::Migration
  def change
    create_table :main_page_slides do |t|
      t.integer :position
      t.integer :project_id
      t.string :slide_image

      t.timestamps
    end
  end
end
