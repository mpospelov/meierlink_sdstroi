class CreateProjectSlides < ActiveRecord::Migration
  def change
    create_table :project_slides do |t|
      t.references :project

      t.date    :date
      t.string  :file

      t.timestamps
    end
  end
end
