class AddPublishedToAdvertisementBlocks < ActiveRecord::Migration
  def change
    add_column :advertisement_blocks, :published, :boolean, default: true
  end
end
