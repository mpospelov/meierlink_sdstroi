class CreateHouses < ActiveRecord::Migration
  def change
    create_table :houses do |t|
      t.integer :project_id
      t.string  :name
      t.timestamps
    end
  end
end
