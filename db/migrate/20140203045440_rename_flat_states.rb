class RenameFlatStates < ActiveRecord::Migration
  def up
    Flat.where(buyed: "куплено").update_all(buyed: "buyed")
    Flat.where(buyed:"забронировано").update_all(buyed: "booked")
    Flat.where(buyed: "не куплено").update_all(buyed: "on_sell")
  end

  def down
  end
end
