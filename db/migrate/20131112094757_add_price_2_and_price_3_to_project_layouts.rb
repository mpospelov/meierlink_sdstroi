class AddPrice2AndPrice3ToProjectLayouts < ActiveRecord::Migration
  def change
    add_column :project_layouts, :price_2, :string
    add_column :project_layouts, :price_3, :string
  end
end
