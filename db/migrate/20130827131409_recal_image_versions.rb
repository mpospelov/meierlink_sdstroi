class RecalImageVersions < ActiveRecord::Migration
  def up
    Project.all.each {|p| p.photo.recreate_versions!}
    MainPageSlide.all.each {|s| s.slide_image.recreate_versions!}
  end

  def down
  end
end
