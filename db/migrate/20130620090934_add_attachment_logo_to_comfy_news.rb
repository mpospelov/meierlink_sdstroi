class AddAttachmentLogoToComfyNews < ActiveRecord::Migration
  def self.up
    change_table :comfy_news do |t|
      t.attachment :logo
    end
  end

  def self.down
    drop_attached_file :comfy_news, :logo
  end
end
