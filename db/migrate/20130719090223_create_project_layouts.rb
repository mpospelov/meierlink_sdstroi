class CreateProjectLayouts < ActiveRecord::Migration
  def change
    create_table :project_layouts do |t|
      t.references :area
      t.string  :name
      t.integer :amount

      t.timestamps
    end

    add_column :project_areas, :photo,      :string
    add_column :project_areas, :project_id, :integer
  end
end
