class RenameProjectIdColumnsInFloors < ActiveRecord::Migration
  def change
    rename_column :floors, :project_id, :house_id
  end
end
