class AddDefaultPositionToMainPageSlides < ActiveRecord::Migration
  def change
    change_column :main_page_slides, :position, :integer, default: 0
  end
end
